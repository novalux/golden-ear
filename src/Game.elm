module Game (Model, init, Action, update, Context, view, makeStep, makeTap, Audio) where

import Color exposing (..)
import Graphics.Collage exposing (..)
import Graphics.Input exposing (customButton)
import Graphics.Element exposing (..)
import Text exposing (Text)
import Random

totalClicks : Int
totalClicks = 4600

type alias Model =
  {
    outerRing : Ring
  , innerRing : Ring
  , userRing : Ring
  , gameOver : Bool
  , playTicks : Int
  , scoringTicks : Int
  , growthFunction : Float -> Float
  , randomGenerator : Random.Generator Float
  , recordingAudio : Audio
  , backgroundAudio : Audio
  }


-- Ring has a radius that is a fraction of the maximum possible, e.g., a value
-- of 1 means the largest possible radius, 0.5 would have half the largest
-- possible radius
type alias Ring = { radius : Float }

type alias Audio =
  {
    isPlaying : Bool
  , volume : Float
  , fromBeginning : Bool
  }


init : Model
init =
  {
    outerRing = { radius = 0.7 }
  , innerRing = { radius = 0.3 }
  , userRing = { radius = 0.50 }
  , gameOver = False
  , playTicks = 0
  , scoringTicks = 0
  , growthFunction = (\val -> abs <| sin (val/50) / 100)
  , randomGenerator = Random.float -1 1
  , recordingAudio = { isPlaying = True, volume = 1, fromBeginning = True }
  , backgroundAudio = { isPlaying = True, volume = 0.4, fromBeginning = True }
  }


type Action
  = Step Float
  | Tap
  | NoOp


makeStep : Float -> Action
makeStep val = Step val


makeTap : Action
makeTap = Tap


update : Action -> Model -> Model
update action model =
  if model.gameOver
     then model
     else model
            |> updateAction action
            |> countTicks
            |> checkForGameOver
            |> controlAudio


updateAction : Action -> Model -> Model
updateAction action model =
  case action of
    Step val ->
      updateStep val model

    Tap ->
      updateTap model

    NoOp ->
      model


updateStep : Float -> Model -> Model
updateStep val model =
  let
    updateByButNotOver1OrNegative add val' = if (val' + add) > 1  || (val' + add) < 0
                                                then val'
                                                else val' + add
  in
    updateUserRingRadius (updateByButNotOver1OrNegative (model.growthFunction (toFloat model.playTicks))) model


updateTap : Model -> Model
updateTap model =
  let
    decreaseByButNotNegative sub val = if (val - sub) < 0 then val else val - sub
  in
    updateUserRingRadius (decreaseByButNotNegative 0.1) model


countTicks : Model -> Model
countTicks model =
    { model |
        playTicks <- model.playTicks + 1,
        scoringTicks <- if userRingBetweenOthers model
                          then model.scoringTicks + 1
                          else model.scoringTicks
    }


checkForGameOver : Model -> Model
checkForGameOver model =
  { model |
      gameOver <- if model.playTicks >= totalClicks
                     then True
                     else False
  }


controlAudio : Model -> Model
controlAudio model =
  let
    recordingAudio' = model.recordingAudio
    backgroundAudio' = model.backgroundAudio
  in
    { model |
        recordingAudio <-
          { recordingAudio' |
              isPlaying <- not model.gameOver,
              volume <- if userRingBetweenOthers model then 1 else 0,
              fromBeginning <- False
          },
        backgroundAudio <-
          { backgroundAudio' |
              isPlaying <- not model.gameOver,
              fromBeginning <- False
          }
    }


userRingBetweenOthers : Model -> Bool
userRingBetweenOthers model =
     model.userRing.radius < model.outerRing.radius
  && model.userRing.radius > model.innerRing.radius


updateUserRingRadius : (Float -> Float) -> Model -> Model
updateUserRingRadius fn model =
  let
    ring = model.userRing
  in
    { model |
        userRing <-
          { ring |
              radius <- fn ring.radius
          }
    }


type alias Context =
  {
    gameOver : Signal.Address ()
  }


userRingLineStyle = { defaultLine | color <- (rgb 114 224 255), width <- 3, join <- Smooth }
otherRingLineStyle = { defaultLine | color <- white, width <- 8 }


view : Context -> (Int, Int) -> Model -> Element
view context dims model =
  if model.gameOver
     then viewGameOver context dims model
     else viewGameOngoing dims model


viewGameOngoing : (Int, Int) -> Model -> Element
viewGameOngoing (w',h') model =
  let
    (w,h) = (toFloat w', toFloat h')
    progressMeterHeight = 4
    progressMeterWidth = (toFloat (model.playTicks) / toFloat (totalClicks)) * w
    growthMeterHeight = 0.1 * (toFloat h')
    growthMeterWidth = (model.growthFunction (toFloat model.playTicks))*100000
    ringsHeight = (toFloat h') - growthMeterHeight - progressMeterHeight
    ringsDims = (toFloat w', ringsHeight)
  in
    layers
      [
        collage w' h'
          [
            toForm <| fittedImage w' h' "assets/imgs/play-bg.jpg"
          , rect w h
              |> gradient (linear (0,h/2) (0, negate (h/2)) [(1, rgba 52 109 209 0.75), (0.5, rgba 96 76 153 0.75)])
          ]
      , collage w' (round progressMeterHeight)
            [
              rect w growthMeterHeight
                |> filled (rgba 96 76 153 0.5)
            , rect progressMeterWidth growthMeterHeight
                |> filled (rgba 114 224 251 0.5)
                |> moveX (progressMeterWidth / 2)
                |> moveX (negate (w/2))
            ]
        `above` collage w' (round ringsHeight)
          [
            -- TODO make something to combine both filling and outlining a
            -- thing, we need it somewhere else too
            circle (absoluteRadiusForRing ringsDims model.outerRing)
              |> filled (rgba 62 146 183 0.5)
          , circle (absoluteRadiusForRing ringsDims model.outerRing)
              |> outlined otherRingLineStyle
          , traced (userRingLineStyle) (viewWiggleUserRing model <| absoluteRadiusForRing ringsDims model.userRing)
          , circle (absoluteRadiusForRing ringsDims model.innerRing)
              |> filled (rgba 96 76 153 0.5)
          , circle (absoluteRadiusForRing ringsDims model.innerRing)
              |> outlined otherRingLineStyle
          , toForm (txt (Text.height 36 >> Text.color (rgb 114 224 251)) <| flip (++) "%" <| toString <| score model)
          ] `above`
          collage w' (round growthMeterHeight)
            [
              rect w growthMeterHeight
                |> filled (rgba 96 76 153 0.5)
            , rect growthMeterWidth growthMeterHeight
                |> filled (rgba 114 224 251 0.5)
            ]
      ]


viewWiggleUserRing : Model -> Float -> Path
viewWiggleUserRing model radius =
  let
    getRandomValue i = fst <| Random.generate model.randomGenerator (Random.initialSeed (model.playTicks + i))
    strength = pi
    offsetTheThing x = x + (getRandomValue (round x)) * strength
    wiggledPoints = List.map (\(x, y) -> (offsetTheThing x, offsetTheThing y)) (pointsOnRing radius)
  in
    path wiggledPoints

pointsOnRing : Float -> List (Float, Float)
pointsOnRing radius =
  let
    nodeCount = 90
    angle i = (i/nodeCount) * 2 * pi
    x a = cos(a) * radius
    y a = sin(a) * radius
  in
    List.foldl (\i pts -> (x (angle i), y (angle i)) :: pts) [] [0..nodeCount]


viewGameOver : Context -> (Int, Int) -> Model -> Element
viewGameOver context (w',h') model =
  let
    scoreRowHeight = 0.6 * (toFloat h')
    summaryRowHeight = 0.3 * (toFloat h')
    buttonRowHeight = 0.1 * (toFloat h')
    isSuccess = if (score model) >= 80 then True else False

    contentWidth = w' - 100

    btn str =
      customButton
        (Signal.message context.gameOver ())
        (image 150 60 ("assets/imgs/btn-" ++ str ++ ".png"))
        (image 150 60 ("assets/imgs/btn-" ++ str ++ "-hover.png"))
        (image 150 60 ("assets/imgs/btn-" ++ str ++ "-hover.png"))
  in
    layers
      [
        collage w' h'
          [
            toForm <| fittedImage w' h' "assets/imgs/play-bg.jpg"
          , toForm <| if isSuccess
                         then viewGameOverSuccessBackground (w',h') model
                         else viewGameOverFailureBackground (w',h') model
          ]
      , collage w' (round scoreRowHeight)
          [
            circle (absoluteRadiusForRing (toFloat w', scoreRowHeight) model.outerRing)
              |> if isSuccess
                    then filled (rgba 12 146 183 0.5)
                    else filled (rgba 255 50 40 0.5)
          , circle (absoluteRadiusForRing (toFloat w', scoreRowHeight) model.outerRing)
              |> outlined otherRingLineStyle
          , toForm <| txt (Text.height 50) <| flip (++) "%" <| toString <| score model
          ]
        `above` collage w' (round summaryRowHeight)
            [
              toForm <| if isSuccess
                           then viewGameOverSuccessText (contentWidth,round summaryRowHeight) model
                           else viewGameOverFailureText (contentWidth,round summaryRowHeight) model
            ]
        `above` collage w' (round buttonRowHeight)
            [
              toForm <| container contentWidth (round buttonRowHeight) midRight <|
                        if isSuccess
                           then btn "continue"
                           else btn "try-again"
            ]
      ]


viewGameOverSuccessBackground : (Int, Int) -> Model -> Element
viewGameOverSuccessBackground (w',h') model =
  let
    (w,h) = (toFloat w', toFloat h')
  in
    collage w' h'
      [
        rect w h
          |> gradient (linear (0,h/2) (0, negate (h/2)) [(1, rgba 60 127 242 0.75), (0.5, rgba 114 224 251 0.75)])
      ]

viewGameOverFailureBackground : (Int, Int) -> Model -> Element
viewGameOverFailureBackground (w',h') model =
  let
    (w,h) = (toFloat w', toFloat h')
  in
    collage w' h'
      [
        rect w h
          |> gradient (linear (0,h/2) (0, negate (h/2)) [(1, rgba 255 50 40 0.75), (0.5, rgba 97 76 153 0.75)])
      ]

viewGameOverSuccessText : (Int, Int) -> Model -> Element
viewGameOverSuccessText (w',h') model =
  let
    (w,h) = (toFloat w', toFloat h')
  in
    flow down
      [
        txt (Text.height 25) "DEBRIEF"
      , collage w' 20 [ rect w 2 |> filled white ]
      , txt (Text.height 25) "The pigeons are coming!"
      ]

viewGameOverFailureText : (Int, Int) -> Model -> Element
viewGameOverFailureText (w',h') model =
  let
    (w,h) = (toFloat w', toFloat h')
  in
    flow down
      [
        txt (Text.height 25) "MISSION FAILURE"
      , collage w' 20 [ rect w 2 |> filled white ]
      , txt (Text.height 25) "I like to listen. I have learned a great deal
      from listening carefully. Most people never listen. - Ernest Hemmingway"
      ]

absoluteRadiusForRing : (Float, Float) -> Ring -> Float
absoluteRadiusForRing (w, h) ring =
  let
    maxRadius = (min w h) / 2 - 10
  in
    maxRadius * ring.radius

score : Model -> Int
score model =
  round <| (toFloat model.scoringTicks / toFloat model.playTicks * 100)

txt : (Text -> Text) -> String -> Element
txt f =
  Text.fromString
  >> Text.color white
  >> Text.monospace
  >> f
  >> centered
