import Color exposing (..)
import Graphics.Collage exposing (..)
import Graphics.Element exposing (..)
import Graphics.Input exposing (customButton)
import Touch
import Text exposing (Text, fromString)
import Time exposing (..)
import Window

import Game


-- MODEL

type alias Model =
  {
    game : Maybe Game.Model
  , isPaused : Bool
  , menuAudio : Game.Audio
  }


type alias Keys = { x:Int, y:Int }


init : Model
init =
  {
    game = Nothing
  , isPaused = False
  , menuAudio = { isPlaying = True, volume = 0.4, fromBeginning = True }
  }


-- UPDATE

type Action
  = NewGame
  | TogglePause
  | GameOver
  | Step Float
  | Tap
  | NoOp

update : Action -> Model -> Model
update action model =
  let menuAudio' = model.menuAudio in
  case action of
    NewGame ->
      { model |
          game <- Just (Game.init)
      }

    TogglePause ->
      { model |
          isPaused <- not model.isPaused
      }

    GameOver ->
      { model |
          game <- Nothing
      }

    Step val ->
      { model |
        game <- Maybe.map (Game.update (Game.makeStep val)) model.game,
        menuAudio <- { menuAudio' | isPlaying <- not (isJust model.game), fromBeginning <- False }
      }

    Tap ->
      { model |
          game <- Maybe.map (Game.update (Game.makeTap)) model.game
      }

    NoOp ->
      model

isJust : Maybe a -> Bool
isJust m =
  case m of
    Just _ -> True
    Nothing -> False


-- VIEW

view : Signal.Address Action -> (Int,Int) -> Model -> Element
view address dims model =
  case model.game of
    Nothing ->
      viewStartScreen address dims model

    Just gameModel ->
      Game.view (Game.Context (Signal.forwardTo address (always GameOver))) dims gameModel


viewStartScreen : Signal.Address Action -> (Int, Int) -> Model -> Element
viewStartScreen address (w', h') model =
  let
    (w,h) = (toFloat w', toFloat h')
    newGameButton =
      customButton
        (Signal.message address NewGame)
        (image 200 80 "assets/imgs/btn-new-game.png")
        (image 200 80 "assets/imgs/btn-new-game-hover.png")
        (image 200 80 "assets/imgs/btn-new-game-hover.png")
  in
    layers
      [
        collage w' h'
          [
            toForm <| fittedImage w' h' "assets/imgs/splash-page.jpg"
          , rect w h
              |> gradient (linear (negate (w/2),h/2) (w/2, negate (h/2)) [(1, rgba 60 127 242 0.8), (0.5, rgba 114 224 255 0.8)])
          , rect w h
              |> gradient (linear (0,h/2) (0, negate (h/2)) [(1, rgba 62 146 183 0.4), (0.5, rgba 96 76 153 0.4)])
          ]
      , container w' h' middle <|
          flow down
            [
              container w' (h'//2) midTop (image (w'//2) (round <| (toFloat w')/2 * 0.2775) "assets/imgs/logo.png")
            , container w' (h'//4) middle newGameButton
            , container w' (h'//16) middle (collage w' h' [ text (Text.color white <| Text.fromString "TIP: keep the pulsing ring in between the others by clicking anywhere") ])
            ]
      ]


viewGameOver : (Int, Int) -> Signal.Address Action -> Model -> Element
viewGameOver (w',h') address model =
  collage w' h' [text (fromString "Game Over")]


-- SIGNALS

actions : Signal.Mailbox Action
actions =
  Signal.mailbox NoOp


model : Signal Model
model =
  let
    step = Signal.map (\t -> Step (t/20)) (fps 30)
    tap = Signal.map (\t -> Tap) (Touch.taps)
  in
    Signal.foldp
    update
    init
    <| Signal.mergeMany
      [
        actions.signal
      , step
      , tap
      ]


main : Signal Element
main =
    Signal.map2 (view actions.address) Window.dimensions model


port controlMenuAudio : Signal Game.Audio
port controlMenuAudio = Signal.map .menuAudio model


port controlGameAudio : Signal Game.Audio
port controlGameAudio = Signal.map (.game >> Maybe.map .recordingAudio >> Maybe.withDefault {isPlaying = False, volume = 0, fromBeginning = True }) model


port controlGameBackgroundAudio : Signal Game.Audio
port controlGameBackgroundAudio = Signal.map (.game >> Maybe.map .backgroundAudio >> Maybe.withDefault {isPlaying = False, volume = 0, fromBeginning = True }) model
