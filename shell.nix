{ pkgs ? (import <nixpkgs> {})
, haskellPackages ? pkgs.haskell-ng.packages.ghc784
}:

let
  modifiedHaskellPackages = haskellPackages.override {
    overrides = self: super: {
      game-jam = self.callPackage ./. {};
    };
  };
in
  modifiedHaskellPackages.game-jam.env
