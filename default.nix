{ mkDerivation, elm-compiler, elm-make, elm-package, elm-reactor
, elm-repl, stdenv
}:
mkDerivation {
  pname = "game-jam";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  buildDepends = [
    elm-compiler elm-make elm-package elm-reactor elm-repl
  ];
  license = stdenv.lib.licenses.agpl3;
}
