# Golden Ear

*Tactical Listening Action*

![Golden Ear Logo](assets/imgs/logo_blue.png)

You take on the role of a spy with slightly better than normal human hearing.
Your mission, focus in on a specific conversation while ignoring distractions.
This is represented by keeping a moving ring between two other rings.

Developed in a weekend for the ICT Game Jam 2015 whose theme was useless
powers.

Note, it currently only runs well in Chrome/Chromium (might be alright in other
WebKit/Blink browsers, but only known to be good in the Chrome family). Here's
[some (older) discussion on the mailing list][elm-discuss-issue] about the
issue and [some hope that the situation is better in Firefox 40][firefox-issue]

[elm-discuss-issue]: https://groups.google.com/d/msg/elm-discuss/j3v7a_75oPI/mkmr-GFK4KcJ
[firefox-issue]: https://bugzilla.mozilla.org/show_bug.cgi?id=1111361#c16
